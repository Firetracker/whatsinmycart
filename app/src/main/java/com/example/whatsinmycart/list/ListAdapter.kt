package com.example.whatsinmycart.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.whatsinmycart.R
import kotlinx.android.synthetic.main.layout_list_item.view.*

class ListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    private var items: List<ListModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ShoplistViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {

            is ShoplistViewHolder -> {
                holder.bind(items.get(position))
            }

        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(cartList: List<ListModel>){
        items = cartList
    }

    class ShoplistViewHolder
    constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){

        val cart_image = itemView.category_item_image
        val cart_title = itemView.category_item_text
        val cart_price = itemView.category_item_price

        fun bind(shopPost: ListModel){

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
               // .circleCrop()

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(shopPost.image)
                .into(cart_image)
            cart_title.setText(shopPost.title)
            cart_price.setText(shopPost.price)

        }

    }

}