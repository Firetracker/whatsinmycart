package com.example.whatsinmycart.list

class ListDataSource {
    companion object {

        fun createDataSet(): ArrayList<ListModel> {
            val list = ArrayList<ListModel>()
            list.add(
                ListModel(
                    "Cucumber",
                    "0.60 €",
                    "https://drive.google.com/uc?id=1Bb9O232JBDs34jWUVZEIZwaQve8DJJPp"
                )
            )
            list.add(
                ListModel(
                    "Watermelon",
                    "2.99 €",
                    "https://drive.google.com/uc?id=15R7OS6Mx6zRRJFAKfwFFdaufhgYTZNV7"
                )
            )
            list.add(
                ListModel(
                    "Banana",
                    "1.25 €",
                    "https://drive.google.com/uc?id=1Q08wJGaJHdETc54SZYgFcsB8-r2sGMOB"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            list.add(
                ListModel(
                    "Avocado",
                    "2 €",
                    "https://drive.google.com/uc?id=1L9glF2GaBiJXn-Z7XrIwFkWAophAOlkq"
                )
            )
            return list
        }
    }
}