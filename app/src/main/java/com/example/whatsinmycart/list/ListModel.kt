package com.example.whatsinmycart.list

data class ListModel (
    var title: String,
    var price: String,
    var image: String
) {

    override fun toString(): String {
        return "BlogPost(title='$title', price='$price', image='$image')"
    }
}