package com.example.whatsinmycart

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.ListFragment
import com.example.whatsinmycart.cart.CartFragment
import com.example.whatsinmycart.graph.GraphFragment
import com.example.whatsinmycart.home.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.login_dialog.view.*

class MainActivity : AppCompatActivity() {
    // Values
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragmentTransaction(HomeFragment())
        setClickListeners()
        SetDialogListener()
    }


    // Functions
    private fun fragmentTransaction(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, fragment)
        transaction.commit()
    }

    private fun setClickListeners() {
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.nav_bar)
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> {
                    fragmentTransaction(HomeFragment())
                }
                R.id.cart -> {
                    fragmentTransaction(CartFragment())
                }
                R.id.graph -> {
                    fragmentTransaction(GraphFragment())
                }
                R.id.list -> {
                    fragmentTransaction(com.example.whatsinmycart.list.ListFragment())
                }
            }
            true
        }
    }

    private fun SetDialogListener() {
        floating_nav_button.setOnClickListener {
            //Inflate the dialog with custom view
            val mDialogView = LayoutInfç0later.from(this).inflate(R.layout.login_dialog, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Login Form")
            //show dialog
            val  mAlertDialog = mBuilder.show()
            //login button click of custom layout
            mDialogView.dialogLoginBtn.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout
                val name = mDialogView.dialogNameEt.text.toString()
                val email = mDialogView.dialogEmailEt.text.toString()
                val password = mDialogView.dialogPasswEt.text.toString()
            }
            //cancel button click of custom layout
            mDialogView.dialogCancelBtn.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
            }
        }
    }

}
